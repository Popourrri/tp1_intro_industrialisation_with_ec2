provider "aws" {
  region = var.region
}

resource "aws_key_pair" "deployer" {
  key_name   = "deployer-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDAMz74P8yHZF7e06E+SjonwksiaYYi564X5/tYWOCdPFc4yIgTn/ud1YIHCZ8d3rjB0DGefuI0DJUyulsLJzhRRd0fLq05vkpExeAdBvgipK6jOnoWtYVdllELNV6hUUFcNktPZlgr1NyJwm0oYe1CsTQndxhsN7vv+zbLvQ+G+tVQkrm1M5bI1etxI65sHXHPsVzN6Cu9GsvFKe0a7L9i5rGDlFb6fsSzR8uu9MIwh93SBLIbkalGsOCdVzlB+0E3B0lotUbWLcZlE+q/OHBzvSZUL8NqN0ZK2nnQrnKEN6ZdWchb5hw9xIWd6Cvk1aS5sqE/zY/rYz4OsG3A+l4eCcxuMuBE9VYOk6dM1msol45GTHyfN1OkriBOVOAtpbKknHvzVQ891IvRvJhyzNr6lvSoYgKUqbYmhom4k+wjLoer+A40+Ij+QoT5RGWYxqkHJ2IUc19NXbF9ZiJxn4jQTmMLMsgDtO7JunPLCPs8SYhc7IHo2TACrmUW5RsfSm0= al@kali"
}

resource "aws_default_vpc" "default" {
  tags = {
    Name = "Default VPC"
  }
}

resource "aws_default_security_group" "default" {
  vpc_id = aws_default_vpc.default.id

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 80
    to_port   = 80
    protocol  = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "my-ec2" {
  ami           = var.ami_id
  instance_type = "t2.micro"
  key_name      = "admin"
  security_groups = ["default"]

  tags = {
    Name = var.tag_name
  }
  depends_on = [aws_key_pair.deployer]
}
